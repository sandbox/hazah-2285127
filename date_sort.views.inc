<?php

/**
 * Implements hook_views_data().
 */
function date_sort_views_data() {
  $data = array();

  $tables = date_views_base_tables();

  foreach ($tables as $base_table => $entity) {
    // The flexible date sort.
    $data[$base_table]['date_sort'] = array(
      'group' => t('Date'),
      'title' => t('Date (!base_table)', array('!base_table' => $base_table)),
      'help' => t('Sort any Views !base_table date field.', array('!base_table' => $base_table)),
      'sort' => array(
        'handler' => 'date_sort_views_handler',
        'empty field name' => t('Undated'),
        'is date' => TRUE,
        //'skip base' => $base_table,
      ),
    );
  }

  return $data;
}

<?php



class date_sort_views_handler extends views_handler_sort_date {
  function init(&$view, &$options) {
    parent::init($view, $options);

    if (empty($this->view->date_info)) {
      $this->view->date_info = new stdClass();
    }
    if (empty($this->view->date_info->date_fields)) {
      $this->view->date_info->date_fields = array();
    }
    $this->view->date_info->date_fields = array_merge($this->view->date_info->date_fields, $this->options['date_fields']);
  }

  // Set default values for the date filter.
  function option_definition() {
    $options = parent::option_definition();
    $options['date_fields'] = array('default' => array());
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $fields = date_views_fields($this->base_table);
    $options = array();
    foreach ($fields['name'] as $name => $field) {
      $options[$name] = $field['label'];
    }

    $form['date_fields'] = array(
      '#title' => t('Date field(s)'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->options['date_fields'],
      '#multiple' => FALSE,
      '#description' => t('Select date field(s) to filter.'),
      '#required' => TRUE,
    );
  }

  function options_validate($form, &$form_state) {
    parent::options_validate($form, $form_state);
    $check_fields = array_filter($form_state['values']['options']['date_fields']);
    if (empty($check_fields)) {
      form_error($form['date_fields'], t('You must select at least one date field for this filter.'));
    }
  }

  function extra_options_submit($form, &$form_state) {
    $form_state['values']['options']['date_fields'] = array_filter($form_state['values']['options']['date_fields']);
  }

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->ensure_my_table();

    $this->get_query_fields();
    if (empty($this->query_fields)) {
      return;
    }

    $fields = array();
    foreach ((array) $this->query_fields as $query_field) {
      $field = $query_field['field'];
      $this->date_handler = $query_field['date_handler'];

      // Respect relationships when determining the table alias.
      if ($field['table_name'] != $this->table || !empty($this->relationship)) {
        $this->related_table_alias = $this->query->ensure_table($field['table_name'], $this->relationship);
      }
      $table_alias = !empty($this->related_table_alias) ? $this->related_table_alias : $field['table_name'];
      $field_name = $table_alias . '.' . $field['field_name'];

      switch ($this->options['granularity']) {
        case 'second':
        default:
          $fields[] = $field_name;
          continue 2;
        case 'minute':
          $formula = views_date_sql_format('YmdHi', $field_name);
          break;
        case 'hour':
          $formula = views_date_sql_format('YmdH', $field_name);
          break;
        case 'day':
          $formula = views_date_sql_format('Ymd', $field_name);
          break;
        case 'month':
          $formula = views_date_sql_format('Ym', $field_name);
          break;
        case 'year':
          $formula = views_date_sql_format('Y', $field_name);
          break;
      }
      $fields[] = $formula;
    }

    // Add the field.
    $this->query->add_orderby(NULL, 'COALESCE(' . implode(', ', $fields) . ')', $this->options['order'], $this->table_alias . '_' . $this->field . '_' . $this->options['granularity']);
  }

  function get_query_fields() {
    $fields = date_views_fields($this->base_table);
    $fields = $fields['name'];
    $this->query_fields = array();
    foreach ((array) $this->options['date_fields'] as $delta => $name) {
      if (array_key_exists($name, $fields) && $field = $fields[$name]) {
        $date_handler = new date_sql_handler($field['sql_type'], date_default_timezone());
        $date_handler->granularity = $this->options['granularity'];
        $date_handler->db_timezone = date_get_timezone_db($field['tz_handling']);
        $date_handler->local_timezone = date_get_timezone($field['tz_handling']);
        $this->query_fields[] = array('field' => $field, 'date_handler' => $date_handler);
      }
    }
  }
}
